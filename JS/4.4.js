"use strict"

if(0){
    let user = {
        name:"Джон",
        age:30
    };

    user.sayHi = function(){
        console.log("Привет!");
    };

    user.sayHi(); // Привет!
//console.log(user);

    for(const prop in user){
        console.log(prop," : ",user[prop]);
    }
    /*  name    :  Джон
        age     :  30
        sayHi   :  [Function (anonymous)]*/

}
if(0){
    let user = {
        name:"Джон",
        age:30,

        sayHi(){
            // console.log( user.name ); // приведёт к ошибке
            console.log(this.name); // а надо так!
        }
    };

    let admin = user;
    user = null; // обнулим переменную для наглядности, теперь она не хранит ссылку на объект.
    admin.sayHi(); // Ошибка! Внутри sayHi() используется user, которая больше не ссылается на объект!
}
if(1){
    let user = {
        name:"Джон"
    };
    let admin = {
        name:"Админ"
    };

    let aza = {
        name:"Aza",
        fufu(){
            console.log(this.name);
        }
    };

    function sayHi(){
        console.log(this.name);
    }

    user.fun = sayHi;
    admin.fun = sayHi;

    // вызовы функции, приведённые ниже, имеют разное значение this
// "this" внутри функции является ссылкой на объект, который указан "перед точкой"
    user.fun(); // Джон  (this == user)
    admin.fun(); // Админ  (this == admin)
    aza.fufu();

    admin['fun'](); // Админ (неважен способ доступа к методу - через точку или квадратные скобки)

}
if(0){
    let user = {
        name:"Джон",
        hi(){
            console.log(this.name);
        }
    };

// разделим получение метода объекта и его вызов в разных строках
    let hei = user.hi;
    hei(); // Ошибка, потому что значением this является undefined
    // Здесь hi = user.hi сохраняет функцию в переменной, и далее в последней строке она вызывается
    // полностью сама по себе, без объекта, так что нет this.

    /**
     * Для работы вызовов типа user.hi(), JavaScript использует трюк – точка '.'
     * возвращает не саму функцию, а специальное значение «ссылочного типа», называемого Reference Type.

     Этот ссылочный тип (Reference Type) является внутренним типом.
     Мы не можем явно использовать его, но он используется внутри языка.

     Значение ссылочного типа – это «триплет»: комбинация из трёх значений (base, name, strict), где:

     base – это объект.
     name – это имя свойства объекта.
     strict – это режим исполнения. Является true, если действует строгий режим (use strict).

     Результатом доступа к свойству user.hi является не функция, а значение ссылочного типа.
     Для user.hi в строгом режиме оно будет таким:

     // значение ссылочного типа (Reference Type)
     (user, "hi", true)

     Когда скобки () применяются к значению ссылочного типа (происходит вызов), то они получают полную
     информацию об объекте и его методе, и могут поставить правильный this (=user в данном случае, по base).

     Ссылочный тип – исключительно внутренний, промежуточный, используемый, чтобы передать информацию
     от точки . до вызывающих скобок ().

     При любой другой операции, например, присваивании hi = user.hi, ссылочный тип заменяется
     на собственно значение user.hi (функцию), и дальше работа уже идёт только с ней. Поэтому
     дальнейший вызов происходит уже без this.

     Таким образом, значение this передаётся правильно, только если функция вызывается напрямую
     с использованием синтаксиса точки obj.method() или квадратных скобок obj['method']()
     (они делают то же самое). Позднее в этом учебнике мы изучим различные варианты решения
     проблемы потери значения this. Например, такие как func.bind().*/
}
if(0){
    function makeUser(){
        return {
            name:"Джон",
            // ref: this,
            ff(){
                console.log(this)
            }
        };
    }
    let user = makeUser();
    console.log(user.ref.name); // Error: Cannot read property 'name' of undefined
}
if(0){
    function makeUser(){// якорь
        return {
            name:"Джон",
            ref(){
                return this;
            }
        };
    }
    let user = makeUser();
    console.log(user.ref().name); // Джон
}
if(0){
    let calculator = {
        sum(){
            return this.a + this.b;
        },
        mul(){
            return this.a * this.b;
        },
        read(){
            this.a = 10;
            this.b = 15;
        }
    };

    calculator.read();
    console.log(calculator.sum());
    console.log(calculator.mul());
}
if(0){
    let ladder = {
        step:0,
        up(){
            this.step++;
            return this;
        },
        down(){
            this.step--;
            return this;
        },
        showStep(){
            console.log(this.step);
            return this;
        }
    }

    ladder.up().up().down().up().up().down().showStep(); // 1
}
if(1){

}



