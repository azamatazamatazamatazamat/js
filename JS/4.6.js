if(0){
// let user = {address:{street:"suka"}}; // пользователь без адреса
    let user = {}; // пользователь без адреса
// let user = null;

    console.log(user?.address?.street)
}
if(1){

    let user1 = {
        firstName: "John"
    };

    let user2 = null;

    console.log( user1?.["firstName"] ); // John
    console.log( user2?.["firstName"] ); // undefined

    // Также мы можем использовать ?. с delete:
    // delete user?.name; // удаляет user.name если пользователь существует
}