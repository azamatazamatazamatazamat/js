// Пустой объект («пустой ящик») можно создать, используя один из двух вариантов синтаксиса:

    if(0){
        let user1 = new Object(); // синтаксис "конструктор объекта"
        let user2 = {};  // синтаксис "литерал объекта"
    }
    if(0){
        /**Литералы и свойства*/
        let user = {     // объект
            name:"John",  // под ключом "name" хранится значение "John"
            age:30        // под ключом "age" хранится значение 30
        };
    }
    if(0){
        // Вычисляемые свойства
        // Мы можем использовать квадратные скобки в литеральной нотации для создания вычисляемого свойства.

        let fruit = "apple";
        let bag = {
            [fruit]: 5, // имя свойства будет взято из переменной fruit
            ["jopa"]: 5,
            jopa2: 5,
            "jopa3": 5,
            // [jopa4]: 5, error
        };
        console.log( bag.apple ); // 5, если fruit="apple"
        console.log( bag.jopa ); // 5, если fruit="apple"
        console.log( bag.jopa2 ); // 5, если fruit="apple"
        console.log( bag.jopa3 ); // 5, если fruit="apple"
// error        // console.log( bag.jopa4 ); // 5, если fruit="apple"
    }

