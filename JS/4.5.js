if(0){
    function User(name){
        this.name = name;
        this.isAdmin = false;
    }

    let user = new User("Вася");

    console.log(user.name); // Вася
    console.log(user.isAdmin); // false
}
if(0){
    // В примере ниже return возвращает объект вместо this:
    function BigUser(){
        this.name = "Вася";
        return {name:"Godzilla"};  // <-- возвращает этот объект
    }

    console.log(new BigUser().name);  // Godzilla, получили этот объект
}
if(0){
    // А вот пример с пустым return (или мы могли бы поставить
    // примитив после return, неважно)
    function SmallUser(){
        this.name = "Вася";
        return; // <-- возвращает this
    }

    console.log(new SmallUser().name);  // Вася
}
if(0){
    // Отсутствие скобок
    // Кстати, мы можем не ставить скобки после new,
    // если вызов конструктора идёт без аргументов.

    let user1 = new User; // <-- без скобок
// то же, что и
    let user2 = new User();

    // Пропуск скобок считается плохой практикой, но синтаксис языка такое позволяет.
}
if(1){
    function User(name){// Функция-конструктор
        this.name = name;

        this.sayHi = function(){
            console.log("Меня зовут: " + this.name);
        };
    }

    let vasya = new User("Вася");
    vasya.sayHi(); // Меня зовут: Вася
    let tre = new User("Azza");
    tre.sayHi(); // Меня зовут: Вася

    /*
    vasya = {
        name: "Вася",
        sayHi: function() { ... }
    }
    */
}
if(0){
    /*
    let calculator = {
        sum(){
            return this.a + this.b;
        },
        mul(){
            return this.a * this.b;
        },
        read(){
            this.a = 10;
            this.b = 15;
        }
    };

    calculator.read();
    console.log(calculator.sum());
    console.log(calculator.mul());
    */
    function Calculator(num1,num2){
        this.num1 = num1;
        this.num2 = num2;

        // this.sayHi = function()
        this.sum = function(){
            return this.num1 + this.num2;
        };

        this.mul = function(){
            return this.num1 * this.num2;
        };

        // this.read = function(){
        //     this.a = 10;
        //     this.b = 15;
        // }
    }

    let calculator = new Calculator(10,10);

    // calculator.read();

    console.log( "Sum=" + calculator.sum() );
    console.log( "Mul=" + calculator.mul() );    
}
if(1){
    function Accumulator(num){
        this.acum =num;

        this.read = function(){
            this.acum++;
            return this;
        }

        this.value = function(){
            this.acum;
            return this;
        }

    }


    let accumulator = new Accumulator(1); // начальное значение 1

    accumulator.read(); // прибавит ввод prompt к текущему значению
    accumulator.read(); // прибавит ввод prompt к текущему значению

    console.log(accumulator.value); // выведет сумму этих значений
}