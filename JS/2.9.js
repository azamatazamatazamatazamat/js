"use strict"
//######################################################################################
/**     Операторы сравнения     */
//######################################################################################
{
    if(false){
        console.log( '2' > 1 ); // true, строка '2' становится числом 2
        console.log( '01' == 1 ); // true, строка '01' становится числом 1
        console.log( true == 1 ); // true
        console.log( false == 0 ); // true

        console.log("---------------");
        let a = 0;
        console.log( Boolean(a) ); // false
        let b = "0";// потому что не пустая строка, а это равно false --> ""
        console.log( Boolean(b) ); // true
        console.log(a == b); // true!
        console.log("---------------");
    }
    if(false){
        /**Сравнение с null и undefined*/
        // При строгом равенстве ===
        console.log( null === undefined ); // false

        // При нестрогом равенстве ==
        console.log( null == undefined ); // true

        console.log( null > 0 );  // (1) false
        console.log( null == 0 ); // (2) false
        console.log( null >= 0 ); // (3) true
    }


}