// Давайте взглянем на ключевые различия между примитивами и объектами.
//
//todo Примитив
//
// Это – значение «примитивного» типа.
/**     Есть 7 примитивных типов:
 *  string,
 *  number,
 *  boolean,
 *  symbol,
 *  null,
 *  undefined
 *  bigint.
 *  */
//todo Объект
//
// Может хранить множество значений как свойства.
//     Объявляется при помощи фигурных скобок {}, например: {name: "Рома", age: 30}. В JavaScript есть и другие виды объектов: например, функции тоже являются объектами.

/** isNaN(value) преобразует значение в число и проверяет является ли оно NaN:*/
/**isFinite(value) преобразует аргумент в число и возвращает true,
 *  если оно является обычным числом, т.е. не NaN/Infinity/-Infinity:*/
let num = 12.3434;
console.log( num.toFixed(2) ); // "12.3"

console.log( isNaN(NaN) ); // true
console.log( isNaN("str") ); // true
console.log("------------------------------------")
console.log( isNaN(11) ); // false
console.log( isNaN("11") ); // false
console.log( isNaN("z0") ); // true
console.log("------------------------------------")
console.log( !isNaN("x11") ); // false
console.log( isFinite("") ); // false

let guestList = `Guests:
 * John
 * Pete
 * Mary
`;

console.log(guestList); // список гостей, состоящий из нескольких строк
